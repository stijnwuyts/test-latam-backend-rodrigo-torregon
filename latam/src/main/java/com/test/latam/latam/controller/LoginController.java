package com.test.latam.latam.controller;

import com.test.latam.latam.dto.LoginDTO;
import org.springframework.web.bind.annotation.*;
import java.net.HttpURLConnection;
import java.net.URL;



@CrossOrigin()
@RestController
@RequestMapping("auth/")
public class LoginController {

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public String login(@RequestHeader(name = "idioma") String idioma,@RequestBody LoginDTO loginDTO) {
        String method = "login";
        System.out.println(method+" init "+loginDTO.getUser());
        String response = "OK";

        try {

            String urlTest = "https://www.ficcion.cl/ws/api/login";
            URL url = new URL(urlTest);
            HttpURLConnection httpCon;
            httpCon = (HttpURLConnection) url.openConnection();
            httpCon.setDoOutput(true);
            httpCon.setRequestProperty("idioma",idioma);
            httpCon.setRequestProperty(
                    "Content-Type", "application/x-www-form-urlencoded");
            httpCon.setRequestMethod("POST");
            httpCon.setRequestProperty("usuario",loginDTO.getUser());
            httpCon.setRequestProperty("password",loginDTO.getPassword());
            httpCon.connect();

            if (httpCon.getResponseCode() <= 299) {
                System.out.println("El usuario está loggeado");
                //todo: guardar en db fecha de log. en JPA user.setLoginDate = new Date(); user.save()
            } else {
                System.out.println("El usuario no está loggeado");
                response = "NOOK";
            }
        } catch (Exception e){
            System.out.println("No pudo conectarse");
            response = "NOOK";
        }

        System.out.println(method+" end "+response);
        return response;
    }

}
